<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:deltaxml="http://www.deltaxml.com/ns/well-formed-delta-v1"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    exclude-result-prefixes="xs map"
    version="3.0">
    
    <xsl:output indent="yes"/>
    <xsl:mode on-no-match="shallow-copy"/>
    <xsl:strip-space elements="*"/>
    
    <xsl:variable name="previousMapCollection" select="collection('.?select=obfuscate-map*.xml') " as="document-node()*"/>
    <xsl:variable name="mapUriList" select="$previousMapCollection!base-uri(.)" as="xs:anyURI*"/>
    <xsl:variable name="mapNumberList" as="xs:integer*" 
        select="for $m in $mapUriList return xs:integer(substring-after(substring-before(xs:string($m), '.xml'), 'obfuscate-map-'))"/>
    <xsl:variable name="maxMapNumber" as="xs:integer" select="if (empty($mapNumberList)) then 0 else max($mapNumberList)"/>
    <xsl:variable name="previousMapFilename" as="xs:string" select="concat('obfuscate-map-', $maxMapNumber, '.xml')"/>
    
    <xsl:variable name="error">
    <message>
        Previous map not found, creating a new map.
    </message>
    </xsl:variable>
    
    <xsl:variable name="previousMapContent" select="if(doc-available($previousMapFilename)) then document($previousMapFilename) else ($error)"/>

    <xsl:variable name="previousWords" as="map(xs:integer, xs:string)">
        <xsl:map>   
            <xsl:for-each select="$previousMapContent/obfuscation-map/words/*">
                <xsl:map-entry key="xs:integer(@pos)" select="xs:string(.)"/>
            </xsl:for-each>
        </xsl:map>
    </xsl:variable>
    
    <xsl:variable name="previousAttrs" as="map(xs:integer, xs:string)">
        <xsl:map>
            <xsl:for-each select="$previousMapContent/obfuscation-map/attrs/*">
                <xsl:map-entry key="xs:integer(@pos)" select="xs:string(.)"/>
            </xsl:for-each>
        </xsl:map>
    </xsl:variable>
    
    <xsl:template match="text()">
        <xsl:value-of select="for $w in tokenize(., ' ') 
            return $previousWords(if (string-length(substring-after($w, 'word')) eq 0) then 0 else xs:integer(substring-after($w, 'word')))" />
    </xsl:template>
    
    <xsl:template match="@* except @deltaxml:*">
        <xsl:attribute name="{name()}" select="for $w in .
            return $previousAttrs(if (string-length(substring-after($w, 'attr')) eq 0) then 0 else xs:integer(substring-after($w, 'attr')))"/>
    </xsl:template>
</xsl:stylesheet>