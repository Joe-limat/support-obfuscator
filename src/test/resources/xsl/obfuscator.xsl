<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:map="http://www.w3.org/2005/xpath-functions/map"
  exclude-result-prefixes="xs map" version="3.0">

  <xsl:output indent="yes"/>
  <xsl:mode on-no-match="shallow-copy"/>
  <xsl:strip-space elements="*"/>
 
  <xsl:variable name="previousMapCollection" select="collection('.?select=obfuscate-map*.xml')" as="document-node()*"/>
  <xsl:variable name="mapUriList" select="$previousMapCollection!base-uri(.)" as="xs:anyURI*"/>
  <xsl:variable name="mapNumberList" as="xs:integer*" 
    select="for $m in $mapUriList return xs:integer(substring-after(substring-before(xs:string($m), '.xml'), 'obfuscate-map-'))"/>
  <xsl:variable name="maxMapNumber" as="xs:integer" select="if (empty($mapNumberList)) then 0 else max($mapNumberList)"/>

  <xsl:param name="newMapFilename" as="xs:string" select="concat('obfuscate-map-', $maxMapNumber  + 1, '.xml')"/>
  <xsl:variable name="previousMapFilename" as="xs:string" select="concat('obfuscate-map-', $maxMapNumber, '.xml')"/> 
  
  <xsl:variable name="error">
    <message>
      Previous map not found, creating a new map.
    </message>
  </xsl:variable>
  
  <xsl:variable name="newMapContent" select="if(doc-available($newMapFilename)) then document($newMapFilename) else ($error)"/>
  <xsl:variable name="previousMapContent" select="if(doc-available($previousMapFilename)) then document($previousMapFilename) else ($error)"/>
   
  <xsl:variable name="previousWords" as="map(xs:string, xs:integer)">
    <xsl:map>
      <xsl:for-each select="$previousMapContent/obfuscation-map/words/*">
        <xsl:map-entry key="xs:string(.)" select="xs:integer(@pos)"/>
      </xsl:for-each>
    </xsl:map>
  </xsl:variable>

  <xsl:variable name="maxPreviousWordValue" as="xs:integer"
    select="if (map:size($previousWords) gt 0) 
    then max(for $k in map:keys($previousWords) return map:get($previousWords, $k))
    else 0"/>

  <xsl:variable name="currentWords" as="map(xs:string, xs:integer)">
    <xsl:map>
      <xsl:for-each select="distinct-values(//text()/tokenize(., ' '))">
        <xsl:map-entry key="xs:string(.)" select="$maxPreviousWordValue + position()"/>
      </xsl:for-each>
    </xsl:map>
  </xsl:variable>

  <xsl:variable name="words" as="map(xs:string, xs:integer)"
    select="map:merge(($previousWords, $currentWords), map{'duplicates':'use-first'})"/>

  <xsl:variable name="previousAttrs" as="map(xs:string, xs:integer)">
    <xsl:map>
      <xsl:for-each select="$previousMapContent/obfuscation-map/attrs/*">
        <xsl:map-entry key="xs:string(.)" select="xs:integer(@pos)"/>
      </xsl:for-each>
    </xsl:map>
  </xsl:variable>

  <xsl:variable name="maxPreviousAttrValue" as="xs:integer"
        select="if (map:size($previousAttrs) gt 0) 
                  then max(for $k in map:keys($previousAttrs) return map:get($previousAttrs, $k))
                else 0"/>

  <xsl:variable name="currentAttrs" as="map(xs:string, xs:integer)">
    <xsl:map>
      <xsl:for-each select="distinct-values(//@*)">
        <xsl:map-entry key="xs:string(.)" select="$maxPreviousAttrValue + position()"/>
      </xsl:for-each>
    </xsl:map>
  </xsl:variable>


  <xsl:variable name="attrs" as="map(xs:string, xs:integer)"
    select="map:merge(($previousAttrs, $currentAttrs), map{'duplicates':'use-first'})"/>

  <xsl:template match="/">
    <xsl:message select="$mapUriList"/>
    <xsl:apply-templates/>
    <xsl:result-document indent="yes" href="{$newMapFilename}" >
      <obfuscation-map>
        <words>
          <xsl:for-each select="map:keys($words)">
            <word pos="{map:get($words, .)}"><xsl:value-of select="."/></word>
          </xsl:for-each>
        </words>
        <attrs>
          <xsl:for-each select="map:keys($attrs)">
            <attr pos="{map:get($attrs, .)}"><xsl:value-of select="."/></attr>
          </xsl:for-each>
        </attrs>
      </obfuscation-map>
    </xsl:result-document>
  </xsl:template>

  <xsl:template match="text()">
    <xsl:value-of select="for $w in tokenize(., ' ') return concat('word', map:get($words, $w))" />
  </xsl:template>

  <xsl:template match="@*">
    <xsl:attribute name="{name()}" select="concat('attr', map:get($attrs, .))"/>
  </xsl:template>

</xsl:stylesheet>