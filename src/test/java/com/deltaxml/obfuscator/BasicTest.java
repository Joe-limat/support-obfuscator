package com.deltaxml.obfuscator;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import net.sf.saxon.s9api.SaxonApiException;


public class BasicTest {
	
	private Basic basic;
	private static final File TestResourcesProject= new File("src/test/resources");
	private static final File TestTarget= new File("target/test-classes");
	private static File inputXML, obfuscatorXSL, deobfuscatorXSL, inputOBS, resultXML, resultOBS, testObfuscation, testDeobfuscation;
	
	/**
	 * Default constructor
	 */
	public BasicTest() {
		basic = new Basic();
		
		obfuscatorXSL= new File(TestResourcesProject, "xsl/obfuscator.xsl");
		deobfuscatorXSL= new File(TestResourcesProject, "xsl/deobfuscator.xsl");
	}

	
	
	@Test
	public void test1() throws IOException, TransformerException, ParserConfigurationException, SAXException, SaxonApiException {
		inputXML= new File(TestTarget, "test1/test1.xml");
	    resultOBS = new File(TestTarget, "test1/test-obfuscated-1.obs");
	    testObfuscation = new File(TestTarget, "test1/obfuscator.xsl");
	    
	    try {
	        Files.copy(obfuscatorXSL.toPath(), testObfuscation.toPath(), StandardCopyOption.REPLACE_EXISTING);
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    
	    basic.obfuscate(inputXML, testObfuscation, resultOBS);
	    
	    Assert.assertTrue(resultOBS.exists());
	    Assert.assertEquals(Files.readAllBytes(Paths.get(TestTarget + "/test1/test-obfuscated-1.obs")),
	    		Files.readAllBytes(Paths.get(TestTarget + "/test1/expected/obfuscated-expected-1.obs")),
	    		"non-obfuscated/obfuscated differ");
	}
	
	@Test
	public void test2() throws IOException, TransformerException, ParserConfigurationException, SAXException, SaxonApiException {
		inputOBS= new File(TestTarget, "test2/test2.obs");
	    resultXML = new File(TestTarget, "test2/test-deobfuscated-2.xml");
	    testDeobfuscation = new File(TestTarget, "test2/deobfuscator.xsl");
	    
	    try {
	        Files.copy(deobfuscatorXSL.toPath(), testDeobfuscation.toPath(), StandardCopyOption.REPLACE_EXISTING);
	    } catch (IOException e) {
	        e.printStackTrace();
	    }

	    basic.deobfuscate(inputOBS, testDeobfuscation, resultXML);
	    
	    Assert.assertEquals(Files.readAllBytes(Paths.get(TestTarget + "/test2/test-deobfuscated-2.xml")),
	    		Files.readAllBytes(Paths.get(TestTarget + "/test2/expected/deobfuscated-expected-2.xml")),
	    		"obfuscated/deobfuscated differ");
	}
	
	@Test
	public void test3() throws IOException, TransformerException, ParserConfigurationException, SAXException, SaxonApiException {
		inputXML= new File(TestTarget, "test3/test3.xml");
	    resultOBS = new File(TestTarget, "test3/test-obfuscated-3.obs");
	    testObfuscation = new File(TestTarget, "test3/obfuscator.xsl");
	    
	    try {
	        Files.copy(obfuscatorXSL.toPath(), testObfuscation.toPath(), StandardCopyOption.REPLACE_EXISTING);
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    
	    basic.obfuscate(inputXML, testObfuscation, resultOBS);
	    
	    Assert.assertTrue(resultOBS.exists());
	    Assert.assertEquals(Files.readAllBytes(Paths.get(TestTarget + "/test3/test-obfuscated-3.obs")),
	    		Files.readAllBytes(Paths.get(TestTarget + "/test3/expected/obfuscated-expected-3.obs")),
	    		"non-obfuscated/obfuscated differ");
	}
	
	@Test
	public void test4() throws IOException, TransformerException, ParserConfigurationException, SAXException, SaxonApiException {
		inputOBS= new File(TestTarget, "test4/test-obfuscated-4.obs");
	    resultXML = new File(TestTarget, "test4/test-deobfuscated-4.xml");
	    testDeobfuscation = new File(TestTarget, "test4/deobfuscator.xsl");

	    try {
	        Files.copy(deobfuscatorXSL.toPath(), testDeobfuscation.toPath(), StandardCopyOption.REPLACE_EXISTING);
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    
	    basic.deobfuscate(inputOBS, testDeobfuscation, resultXML);
	    
	    Assert.assertEquals(Files.readAllBytes(Paths.get(TestTarget + "/test4/test-deobfuscated-4.xml")),
	    		Files.readAllBytes(Paths.get(TestTarget + "/test4/expected/deobfuscated-expected-4.xml")),
	    		"obfuscated/deobfuscated differ");
	}
}
