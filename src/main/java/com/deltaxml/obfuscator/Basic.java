package com.deltaxml.obfuscator;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamSource;

import org.xml.sax.SAXException;

import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.Serializer;
import net.sf.saxon.s9api.XsltExecutable;
import net.sf.saxon.s9api.XsltTransformer;

public class Basic {

    private Processor p;
    private XsltTransformer xt;
    private XsltExecutable xe;
    
    public Basic() {
    	p = new Processor(false);
    	xe= null;
    }
    
    /**
     * Calls common to obfuscate files
     * 
     * @param inputXML - The input XML file to be obfuscated
     * @param inputXSL - The obfuscate XSL that will be called to obfuscate the file
     * @param requestOutputFile - The obfuscated file that will be output
     */
	public final void obfuscate(File inputXML, File inputXSL, File requestOutputFile)
			throws IOException, TransformerException, ParserConfigurationException, SAXException, SaxonApiException {
		
		common(inputXML, inputXSL, requestOutputFile);
	}
	
	/**
	 * Calls common to deobfuscate files
	 * 
	 * @param inputOBS - The obfuscated file that will be deobfuscated
	 * @param inputXSL - The deobfuscate XSL that will be called to deobfuscate the file
	 * @param requestOutputFile - The deobfuscated file that will be output
	 */
	public void deobfuscate(File inputOBS, File inputXSL, File requestOutputFile) 
			throws IOException, TransformerException, ParserConfigurationException, SAXException, SaxonApiException {
		
		common(inputOBS, inputXSL, requestOutputFile);
	}
	
	/**
	 * Generic method used in obfuscation and deobfuscation methods
	 * 
	 * @param inputA - XML file
	 * @param inputB - XSL (obfuscator/deobfuscator) file
	 * @param result - result file containing obfuscated/deobfuscated content
	 */
	private void common(File inputA, File inputB, File result) throws SaxonApiException {
		
		xe = p.newXsltCompiler().compile(new StreamSource(inputB));
		
		Serializer out = p.newSerializer(result);
		out.setOutputProperty(Serializer.Property.METHOD, "xml");
		out.setOutputProperty(Serializer.Property.INDENT, "yes");
		xt = xe.load();
		xt.setSource(new StreamSource(inputA));
		xt.setDestination(out);
		xt.transform();
	}
}
