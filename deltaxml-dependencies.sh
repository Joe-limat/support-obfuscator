#!/bin/bash
if [ -f /usr/libexec/java_home ]; then
export JAVA_HOME=`/usr/libexec/java_home -v1.7`
else
export JAVA_HOME=/usr/local/java/jdk1.8.0_102
fi
export PATH=/usr/local/java/apache-maven-3.3.9/bin:$PATH
mvn install:install-file -Dfile=/usr/local/deltaxml/DeltaXML-XML-Compare-10_0_0_j/resolver.jar -DgroupId=com.deltaxml -DartifactId=resolver -Dversion=10.0.0 -Dpackaging=jar
mvn install:install-file -Dfile=/usr/local/deltaxml/DeltaXML-XML-Compare-10_0_0_j/deltaxml-10.0.0.jar -DgroupId=com.deltaxml -DartifactId=deltaxml -Dversion=10.0.0 -Dpackaging=jar
mvn install:install-file -Dfile=/usr/local/deltaxml/DeltaXML-XML-Compare-10_0_0_j/flexlm.jar -DgroupId=com.deltaxml -DartifactId=flexlm -Dversion=10.0.0 -Dpackaging=jar
mvn install:install-file -Dfile=/usr/local/deltaxml/DeltaXML-XML-Compare-10_0_0_j/EccpressoAll.jar -DgroupId=com.deltaxml -DartifactId=EccpressoAll -Dversion=10.0.0 -Dpackaging=jar
mvn install:install-file -Dfile=/usr/local/deltaxml/DeltaXML-XML-Compare-10_0_0_j/xercesImpl.jar -DgroupId=com.deltaxml -DartifactId=xercesImpl -Dversion=10.0.0 -Dpackaging=jar
mvn install:install-file -Dfile=/usr/local/deltaxml/DeltaXML-XML-Compare-10_0_0_j/xml-apis.jar -DgroupId=com.deltaxml -DartifactId=xml-apis -Dversion=10.0.0 -Dpackaging=jar
mvn install:install-file -Dfile=/usr/local/deltaxml/DeltaXML-XML-Compare-10_0_0_j/icu4j.jar -DgroupId=com.deltaxml -DartifactId=icu4j -Dversion=10.0.0 -Dpackaging=jar