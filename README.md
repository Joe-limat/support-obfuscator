## Support-Obfuscator

The Obfuscator helps to protect your confidential data by creating a key-value map which allows the deobfuscator to recreate the original data in any result file. As you run the obfuscator on your file, it replaces each word with a placeholder and a unique word ID. This is stored in a key-value map, ensuring that only you can run the Deobfuscator on your files to retrieve your sensitive data.


**`NOTE!`**
`Only node and attribute values are obfuscated, names remain unchanged.`


![Obfuscation and Deobfuscation diagram workflow](workflow-diagram.png)

---

#### Obfuscate Files

1. Make sure all xml files you wish to obfuscate and obfuscator.xsl are all in the same directory.
2. Make sure you have SaxonHE9.8 or higher installed on your machine and have the file path.
3. Open up a terminal window in the same directory as the xml file(s) and type:
```
java -jar /Path/To/Saxon-HE/9.8.0-4/Saxon-HE-9.8.0-4.jar -s:unobfuscatedFile.xml -xsl:obfuscator.xsl -o:result.obs
```

Make sure you keep the ‘obfuscate-map’ xml file safe and know which input file(s) it belongs to.

#### Deobfuscate Files

1. Make sure the obfuscated file and deobfuscator.xsl are in the same directory, along with the appropriate ‘obfuscate-map’ file from your obfuscation process. (Usually, it will be obfuscate-map with the greatest suffix)
2. Make sure you have SaxonHE9.8 or higher installed on your machine and have the file path.
3. Open up a terminal window in the same directory as the xml file(s) and type:
```
java -jar /Path/To/Saxon-HE/9.8.0-4/Saxon-HE-9.8.0-4.jar -s:obfuscatedFile.obs -xsl:deobfuscator.xsl -o:result.xml
```

Replace obfuscatedFile.obs with the name of the file that you wish to deobfuscate.  Use an appropriate name for the result.xml file.

---

**`NOTE!`**
`Do not obfuscate files in the same directory where you have already obfuscated files that have no correspondence with what you are obfuscating now. This will cause the obfuscator to look at the previous map and include all of those words in the new map. This shouldn’t break the obfuscation/deobfuscation process, but can cause map files to be unnecessarily large.`